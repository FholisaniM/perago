package com.perago.techtest.test;

import java.io.Serializable;

import org.junit.Before;
import org.junit.Test;

import com.perago.techtest.Diff;
import com.perago.techtest.DiffException;
import com.perago.techtest.DiffImpl;

public class DiffEngineTest {

	private DiffImpl diffEngine;

	@Before
	public void setup() {
		diffEngine = new DiffImpl();
	}
	
    @Test
    public void createTest() throws DiffException {

    	  // calling garbage collector 
        System.gc(); 
        Person original = new Person();
     

        Person modified = new Person.PersonBuilder("Fred", "Smith").setFriend(null).setNickNames(null).setPet(null).build();
 

        diffEngine.calculate(original, modified);
        diffEngine.apply(original, diffEngine.calculate(original, modified));
  

    };
    
    @Test
    public void deleteTest() throws DiffException {
    	  // calling garbage collector 
        System.gc(); 
    	
        Person modified = new Person();
     

        Person original = new Person.PersonBuilder("Fred", "Smith").setFriend(null).setNickNames(null).setPet(null).build();
 

        diffEngine.calculate(original, modified);
        diffEngine.apply(original, diffEngine.calculate(original, modified));

    }
    
    @Test
    public void updateTest() throws DiffException {

    	  // calling garbage collector 
       System.gc(); 
        Person modified = new Person.PersonBuilder("Fred", "Jones").setFriend(null).setNickNames(null).setPet(null).build();
     

       Person original = new Person.PersonBuilder("Fred", "Smith").setFriend(null).setNickNames(null).setPet(null).build();
 

        diffEngine.calculate(original, modified);
        diffEngine.apply(original, diffEngine.calculate(original, modified));

    }
    
    @Test
    public void updateFriendTest() throws DiffException {
    	  // calling garbage collector 
        System.gc(); 
    	Person friendModified = new Person.PersonBuilder("Tom", "Brown").setFriend(null).setNickNames(null).setPet(null).build();
        Person modified = new Person.PersonBuilder("Fred", "Jones").setFriend(friendModified).setNickNames(null).setPet(null).build();
     

        Person original = new Person.PersonBuilder("Fred", "Smith").setFriend(null).setNickNames(null).setPet(null).build();
        
 

        diffEngine.calculate(original, modified);
        diffEngine.apply(original, diffEngine.calculate(original, modified));

   }

    
    
    @Test
    public void updateFriendModifiedOridinalPetTest() throws DiffException {

    	  // calling garbage collector 
        System.gc(); 
    	Person friendModified = new Person.PersonBuilder("Tom", "Brown").setFriend(null).setNickNames(null).setPet(null).build();
    	Pet pet = new Pet.PetBuilder("Dog", "Spot").build();
        Person modified = new Person.PersonBuilder("Fred", "Jones").setFriend(friendModified).setNickNames(null).setPet(pet).build();
     

        Person friendOriginal= new Person.PersonBuilder("Tom", "Brown").setFriend(null).setNickNames(null).setPet(null).build();
        Person original = new Person.PersonBuilder("Fred", "Smith").setFriend(friendOriginal).setNickNames(null).setPet(null).build();
        
 

        diffEngine.calculate(original, modified);
        diffEngine.apply(original, diffEngine.calculate(original, modified));

    }
}
