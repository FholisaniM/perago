package com.perago.techtest;

import java.io.Serializable;
import java.util.List;

import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Change;
import org.javers.core.diff.changetype.ValueChange;

/**
 * The object representing a diff. Implement this class as you see fit.
 *
 */
public class Diff<T extends Serializable> {

	private List<T> change;

	private Object bean;
	private String objectStatus;
	private String propertyName;
	private String originalFieldValue;
	private String modifiedFieldValue;

	public Object getBean() {
		return bean;
	}

	public void setBean(Object bean) {
		this.bean = bean;
	}

	public String getObjectStatus() {
		return objectStatus;
	}

	public void setObjectStatus(String objectStatus) {
		this.objectStatus = objectStatus;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getOriginalFieldValue() {
		return originalFieldValue;
	}

	public void setOriginalFieldValue(String originalFieldValue) {
		this.originalFieldValue = originalFieldValue;
	}

	public String getModifiedFieldValue() {
		return modifiedFieldValue;
	}

	public void setModifiedFieldValue(String modifiedFieldValue) {
		this.modifiedFieldValue = modifiedFieldValue;
	}

	public List<T> getChange() {
		return change;
	}

	public void setChange(List<T> change) {
		this.change = change;
	}

}
