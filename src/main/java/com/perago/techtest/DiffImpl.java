package com.perago.techtest;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Change;
import org.javers.core.diff.changetype.ValueChange;



public class DiffImpl implements DiffEngine, DiffRenderer{

	public String render(Diff<?> diff) throws DiffException {
		// TODO Auto-generated method stub
		return null;
	}

	public <T extends Serializable> T apply(T original, Diff<?> diff) throws DiffException {
		// TODO Auto-generated method stub
		 String humanReadableFormat = null;

	        if (diff.getObjectStatus().equals(Status.Update.toString())) {

	            humanReadableFormat = diff.getObjectStatus() + " : " + diff.getPropertyName() + " from \"" + diff.getOriginalFieldValue() + "\" to \"" + diff.getModifiedFieldValue() + "\"";
	           
	        } else if (diff.getObjectStatus().equals(Status.Delete.toString())) {

	            humanReadableFormat = diff.getObjectStatus() + " : " + diff.getPropertyName();

	        } else if (diff.getObjectStatus().equals(Status.Create.toString())) {

	            String propertyValue = (diff.getModifiedFieldValue() != null) ? "\"" + diff.getModifiedFieldValue() + "\"" : diff.getModifiedFieldValue();
	            humanReadableFormat = diff.getObjectStatus() + " : " + diff.getPropertyName() + " as " + propertyValue;
	        }
	        System.out.println(diff.getChange().size());
	        System.out.println(humanReadableFormat);
	        System.out.println(diff.getChange());
	       
	        return (T) humanReadableFormat;
	}

	public <T extends Serializable> Diff<T> calculate(T original, T modified) throws DiffException {
		// TODO Auto-generated method stub
		

	
	
		Diff diff = new Diff<Serializable>();
		//given
	    Javers javers = JaversBuilder.javers().build();
	    //when
	    org.javers.core.diff.Diff diffence = javers.compare(original, modified);
	    if (!Objects.isNull(original) && !Objects.isNull(modified)) {
	   //	 System.out.println(Status.Update.toString() + "  : " + original.getClass().getSimpleName());
	   	 diff.setObjectStatus(Status.Update.toString());
    	 diff.setChange(diffence.getChanges());
	    }else if (!Objects.isNull(modified)) {
	    //	 System.out.println(Status.Create.toString() + "  : " + original.getClass().getSimpleName());
	    	 diff.setObjectStatus(Status.Create.toString());
	    	 diff.setChange(diffence.getChanges());
	    }else if(!Objects.isNull(original)) {
	    	 diff.setObjectStatus(Status.Delete.toString());
	    	// System.out.println(Status.Delete.toString() + "  : " + original.getClass().getSimpleName());
	    }
	   

	  
	    return diff;
	}

}
